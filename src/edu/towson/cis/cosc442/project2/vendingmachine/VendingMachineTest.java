package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;

public class VendingMachineTest {

	/**
	 * Declaring necessary test objects for the test cases to use
	 */
	VendingMachine vm1;
	VendingMachine vm2;
	VendingMachine vm3;
	VendingMachineItem popTart;
	VendingMachineItem beefJerky;
	VendingMachineItem key;
	

	/**
	 * Initializes the necessary test objects for the test cases to use.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		vm1 = new VendingMachine();
		vm2 = new VendingMachine();
		vm3 = new VendingMachine();
		popTart = new VendingMachineItem("Pop Tart", 1.75);
		beefJerky = new VendingMachineItem("Beef Jerky", 2.00);
		key = new VendingMachineItem(popTart.getName(), popTart.getPrice());
		vm3.addItem(popTart, "A");
	}
	
	/**
	 * Tests to see if item can be properly inserted into a vending machine and found
	 */
	
	@Test
	public void testAddItemSuccessA() {
	

		boolean foundItem = false;
		vm2.addItem(new VendingMachineItem(popTart.getName(), popTart.getPrice()), "A");
		for(VendingMachineItem item: vm2.getItems()) {
			if(item != null && item.getPrice() == key.getPrice() && item.getName() == key.getName()) foundItem = true; 
		}
		
		assertTrue(foundItem);
		
	}
	
	@Test
	public void testAddItemSuccessB() {

		vm2.addItem(new VendingMachineItem(popTart.getName(), popTart.getPrice()), "B");
		boolean foundItem = false;
		for(VendingMachineItem item: vm2.getItems()) {
			if(item != null && item.getPrice() == key.getPrice() && item.getName() == key.getName()) foundItem = true; 
		}
		
		assertTrue(foundItem);
		
	}
	@Test
	public void testAddItemSuccessC() {
		
		vm2.addItem(new VendingMachineItem(popTart.getName(), popTart.getPrice()), "C");
		boolean foundItem = false;
		for(VendingMachineItem item: vm2.getItems()) {
			if(item != null && item.getPrice() == key.getPrice() && item.getName() == key.getName()) foundItem = true; 
		}
		assertTrue(foundItem);
	}
	
	@Test
	public void testAddItemSuccessD() {
		vm2.addItem(new VendingMachineItem(popTart.getName(), popTart.getPrice()), "D");
		boolean foundItem = false;
		for(VendingMachineItem item: vm2.getItems()) {
			if(item != null && item.getPrice() == key.getPrice() && item.getName() == key.getName()) foundItem = true; 
		}
		assertTrue(foundItem);
	}
	

	@Test(expected = VendingMachineException.class)
	public void testAddItemFailure1() {	
		vm2.addItem(new VendingMachineItem(popTart.getName(), popTart.getPrice()), "F");		
	}
	
	@Test(expected = VendingMachineException.class)
	public void testAddItemFailure2() {
		vm3.addItem(beefJerky, "A");
	}
	
	/**
	 * Tests to see if item can be properly removed from a vending machine and found
	 */

	@Test
	public void testRemoveItemSuccess() {
		vm2.addItem(popTart, "A");
		vm2.removeItem("A");
	}
	
	@Test(expected = VendingMachineException.class)
	public void testRemoveItemFailure1() {
		vm2.removeItem("F");
	}
	@Test(expected = VendingMachineException.class)
	public void testRemoveItemFailure2() {
		vm2.removeItem("A");
	}
	
	

	/**
	 * Tests to see if money can be properly added to balance of vending machine
	 */
	@Test
	public void testInsertMoneySuccess() {
		vm1.insertMoney(10);
		assertEquals(10, vm1.getBalance(), .01);
	}
	
	@Test(expected = VendingMachineException.class)
	public void testInsertMoneyFailure() {
		vm1.insertMoney(-1.0);
	}
	
	
	
	/**
	 * Tests to see balance can be properly retrieved from the vending machine
	 */
	@Test
	public void testGetBalance() {

		vm1.insertMoney(10.5);
		assertEquals(10.5, vm1.getBalance(), .01);
		
		vm1.insertMoney(15.49);
		assertEquals(25.99, vm1.getBalance(), .01);
	}

	
	/**
	 * Tests to see if purchases can be properly made from the vending machine
	 */
	@Test
	public void testMakePurchaseSuccess() {
		vm1.insertMoney(2.00);
		vm1.addItem(new VendingMachineItem(beefJerky.getName(), beefJerky.getPrice()), "A");
		boolean hasValidBalance = vm1.makePurchase("A");
	
		assertTrue(hasValidBalance);
		
	}
	
	@Test
	public void testMakePurchaseFailure() {
		vm1.insertMoney(1.75);
		vm1.addItem(new VendingMachineItem(beefJerky.getName(), beefJerky.getPrice()), "A");
		
		boolean hasValidBalance = (vm1.makePurchase("A"));
		assertFalse(hasValidBalance);
	}	

	
	/**
	 * Tests to see if change can be properly returned from vending machine
	 */
	@Test
	public void testReturnChange() {
		vm1.insertMoney(10.15);
		double change = vm1.returnChange();
		
		assertEquals(10.15, change, .01);
		assertEquals(0.0, vm1.getBalance(), .01);
	}
	
	@After
	public void tearDown(){
		vm1 = null; 
		vm2 = null;
	}

}
