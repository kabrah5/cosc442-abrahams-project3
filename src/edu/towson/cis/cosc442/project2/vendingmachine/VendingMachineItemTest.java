package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VendingMachineItemTest {

	/**
	 * Declaring necessary test object for the test cases to use
	 */
	VendingMachineItem vmi1;
	
	/**
	 * Initializes the necessary test object for the test cases to use.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		vmi1 =  new VendingMachineItem("Pringles", 2.00);
	}

	/**
	 * Test to see if vending machine item can be properly instantiated
	 */
	@Test
	public void testVendingMachineItemSuccess() {
		VendingMachineItem vmi = new VendingMachineItem("Cheeze-itz", 1.50);
	}
	
	@Test(expected = VendingMachineException.class)
	public void testVendingMachineItemFailure() {
		VendingMachineItem vmi = new VendingMachineItem("Cheeze-itz", -1.50);
	}
	

	/**
	 * Tests to see if name of vending machine item can be properly retrieved
	 */
	@Test
	public void testGetName() {
		assertEquals("Pringles", vmi1.getName());
	}
	
	/**
	 * Tests to see price of vending machine item can be properly retrieved
	 */
	@Test
	public void testGetPrice() {
		assertEquals(2.00, vmi1.getPrice(), .01);
	}
	

}
